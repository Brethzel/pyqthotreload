import os
import os.path
import shutil
from shutil import copyfile
import sys

from PyQt5 import QtCore, QtQuick

class HotReload(QtQuick.QQuickView):
    def __init__(self, mainWindow):
        QtQuick.QQuickView.__init__(self)
        self.watcher = QtCore.QFileSystemWatcher()
        self.initScanning()

    def fileChanged(self, path):
        print("[hotreload] " + QtCore.QDateTime.currentDateTime().toString("hh:mm:ss") + " change detected on " + path)
        self.reload()

    def initScanning(self):
        paths = []

        iterator = QtCore.QDirIterator(".", { "*.qml" }, QtCore.QDir.Files, QtCore.QDirIterator.Subdirectories)

        while iterator.hasNext():
            iterator.next()
            paths.append(iterator.filePath())
            print("[hotreload] watching: " + iterator.filePath())

        self.watcher = QtCore.QFileSystemWatcher(paths)
        self.watcher.fileChanged.connect(self.fileChanged)

    def reload(self):
        src = self.source()
        self.setSource(QtCore.QUrl())
        self.engine().clearComponentCache()
        self.setSource(src)
        print("[hotreload] " + QtCore.QDateTime.currentDateTime().toString("hh:mm:ss") + " reloaded")
