# PyQtHotReload

This is a very simple PyQt class that enables hotreload for QML based applications.

I usually git submodule this into a "HotReload" Folder, then use the following to instantiate my QML application.

Of course, this code should be removed/disabled in production.

```python
app = QtGui.QGuiApplication(sys.argv)
engine = QtQml.QQmlApplicationEngine()
view = HotReload.HotReload.HotReload(app)

# register types if any

# set context properties if any

view.setSource(QtCore.QUrl('main.qml'))
view.engine().quit.connect(QtCore.QCoreApplication.quit)
view.show()

sys.exit(app.exec_())
```
